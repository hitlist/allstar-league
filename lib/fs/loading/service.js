'use strict'
/**
 * Loader to autodiscover services modules
 * @module allstar-auth/lib/fs/loading/service
 * @author Eric Satterwhiet
 * @since 0.1.0
 **/
const Loader = require('gaz/fs/loader')

module.exports = new Loader({
  searchpath: 'services'
, filepattern: /\.js$/
, recurse: true
})
