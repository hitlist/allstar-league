'use strict'

/**
 *
 * @module allstar-league/lib/hemera
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires nats-hemera
 * @requires hemera-stats
 * @requires hemera-jaeger
 * @requires @allstar/parse-hosts
 * @requires @allstar/hemera-acl
 **/

const conf = require('keef')
const nats = require('nats')
const Hemera = require('nats-hemera')
const stats = require('hemera-stats')
const Jaeger = require('hemera-jaeger')
const parse = require('@allstar/parse-hosts')
const acl = require('@allstar/hemera-acl')

const {connection} = require('zim/lib/db')
const nats_config = conf.get('nats')
const nc = nats.connect({
  ...nats_config
, servers: parse(nats_config.servers)
})


const hemera = new Hemera(nc, {
  logLevel: conf.get('log:stdout:level')
, childLogger: true
, tag: 'league'
})

hemera.use(Jaeger, {
  serviceName: 'allstar-league'
})

hemera.use(stats)
hemera.use(acl)

process.once('SIGTERM', onSignal)
process.once('SIGINT', onSignal)
module.exports = hemera

function onSignal() {
  hemera.log.info('shutdown signal received')
  hemera.log.info('shutdown hemera')
  hemera.close()
  nc.close(() => {})
  connection.disconnect('allstar')
  hemera.log.info('shutdown database connection')
  process.exit(0)
}
