'use strict'

module.exports = {
  "league": {
    "databases": {
      "allstar": {
        "host": "0.0.0.0"
      , "db": "allstar_league"
      , "default": true
      , "driver": "rethinkdb"
      , "port": 28015
      }
    }
  }
, "nats":{
    "user": null
  , "pass": null
  , "servers": [
      "nats://0.0.0.0:4222"
    , "nats://0.0.0.0:4223"
    , "nats://0.0.0.0:4224"
    ]
  }
}
