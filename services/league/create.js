'use strict'

/**
 * Create a new Leauge
 * @module allstar-league/services/league/create
 * @author Eric Sattewrhite
 * @since 1.0.0
 * @requires allstar-league/lib/hemera
 * @requires allstar-league/models/league/league
 * @auth league:league:create
 **/

const hemera = require('../../lib/hemera')
const League = require('../../models/league/league')

module.exports =
hemera.add({
  topic: 'league'
, cmd: 'create'
, version: 'v1'
, auth$: {
    permissions: 'league:league:create'
  }
}, async function (opts) {

  opts.founders = await verifyFounders(opts.founders)
  const league = await new League(opts).save()
  this.log.info('creating new league', opts)
  this.log.info('new league created: ', league.league_name)
  this.act({
    topic: 'activity'
  , cmd: 'create'
  , version: 'v1'
  , pubsub$: true
  , action: 'create'
  , content_type: League.contentType()
  , content_id: league.pk()
  , creator_id: this.user$.id || '00000000-0000-0000-0000-000000000000'
  , creator_type: this.user$.type || 'system'
  , season: new Date().getFullYear()
  , extra: {
      league_name: league.league_name
    , abbr: league.abbr
    }
  })
  return league
})

async function verifyFounders(founders = []) {
  if (!founders.length) return Promise.resolve([])

  const keys = founders.map((founder) => {
    return typeof founder == 'object' ? founder.id : founder
  })

  const {data} = await hemera.act({
    topic: 'user'
  , cmd: 'mget'
  , version: 'v1'
  , keys: keys
  })
  return data
}
