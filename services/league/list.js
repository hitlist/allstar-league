'use strict'

/**
 * List existing leagues
 * @module allstar-league/services/league/list
 * @author Eric Sattewrhite
 * @since 1.0.0
 * @requires allstar-league/lib/hemera
 * @requires allstar-league/models/league/league
 * @requires @allstar/req-builder
 * @auth league:league:create
 **/

const {connection} = require('zim/lib/db')
const {filter, toResponse} = require('@allstar/reql-builder')
const hemera = require('../../lib/hemera')
const League = require('../../models/league/league')
const r = connection.default.r

const table = League.getTableName()

module.exports =
hemera.add({
  topic: 'league'
, cmd: 'list'
, version: 'v1'
}, (req) => {
  const {limit = 25, offset= 0, filters = {}} = req
  return toResponse(r, table, {
    filters
  , orderby: '-league_name'
  , orderby_index: 'league_name'
  , limit
  , offset
  })
})
