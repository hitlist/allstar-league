'use strict'

const hemera = require('../../lib/hemera')
const League = require('../../models/league/league')
const Team = require('../../models/league/team/team')
const Conference = require('../../models/league/conference')
const Season = require('../../models/season')
const LeagueTeam = require('../../models/league/team/league-team')

module.exports = 
hemera.add({
  topic: 'league'
, cmd: 'add-team'
, version: 'v1'
, auth$: {
    permissions: 'league:member:add'
  }
}, async function(req) {
  const [league, team, conference, season] = await Promise.all([
    League.get(req.league).run()
  , Team.get(req.team).run()
  , Conference.get(req.conference).run()
  , Season.get(req.season).run()
  ])

  return await LeagueTeam({
    league_id: league.pk()
  , team_id: team.pk()
  , conference_id: conference.pk()
  , season: season.pk()
  }).save()
})
