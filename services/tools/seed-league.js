'use strict'

/**
 * Create a new Leauge
 * @module allstar-league/services/league/create
 * @author Eric Sattewrhite
 * @since 1.0.0
 * @requires allstar-league/lib/hemera
 * @requires allstar-league/models/league/league
 * @auth league:league:create
 **/

const hemera = require('../../lib/hemera')
const League = require('../../models/league/league')

module.exports =
hemera.add({
  topic: 'tools'
, cmd: 'seed-league'
, version: 'v1'
, auth$: {
    permissions: 'league:league:create'
  }
}, async function (req) {
  this.log.info('creating new league', req)
  const founder = await this.act({
    topic: 'user'
  , cmd: 'get'
  , version: 'v1'
  , user_id: req.founder
  })
  console.log(founder)
  const league = await new League({
    league_name: req.name
  , abbr: req.abbr
  , founded_at: new Date(req.founded)
  , founders:[{
      id: req.founder
    , name: founder.name
    }]
  }).save()
  this.log.info('new league created: ', league.league_name)
  return league
})
