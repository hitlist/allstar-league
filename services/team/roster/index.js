'use strict'

module.exports = {
  addmember: require('./add-member')
, listMember: require('./list-member')
, get: require('./get-member')
, removeMember: require('./remove-member')
}
