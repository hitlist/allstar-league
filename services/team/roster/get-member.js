'use strict'

const {connection} = require('zim/lib/db')
const {filter} = require('@allstar/reql-builder')
const sort = require('@allstar/reql-builder/lib/sort')
const TeamMember = require('../../../models/league/team/team-member')
const hemera = require('../../../lib/hemera')
const r = connection.default.r
const table = TeamMember.getTableName()

module.exports =
hemera.add({
  topic: 'roster'
, cmd: 'get-player'
, version: 'v1'
, auth$: {
    permissions: 'auth:user:read'
  }
}, async function(req) {
  if (!req.user_id) return null
  this.log.debug(`looking up user ${req.user_id}`)
  return await TeamMember.getAll(req.user_id).getJoin().nth(0)
})
