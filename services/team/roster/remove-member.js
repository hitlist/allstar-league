'use strict'

const {connection} = require('zim/lib/db')
const {filter} = require('@allstar/reql-builder')
const sort = require('@allstar/reql-builder/lib/sort')
const TeamMember = require('../../../models/league/team/team-member')
const hemera = require('../../../lib/hemera')
const r = connection.default.r
const table = TeamMember.getTableName()

module.exports =
hemera.add({
  topic: 'roster'
, cmd: 'remove-player'
, version: 'v1'
, auth$: {
    permissions: 'league:roster:delete'
  }
}, async function (req) {
  if (!req.user_id) return null
  this.log.debug(`remove user ${req.user_id}`)
  return await TeamMember.filter({user: req.user_id}).delete()
})
