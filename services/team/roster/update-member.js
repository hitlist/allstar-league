'use strict'

const {connection} = require('zim/lib/db')
const {HemeraError} = require('nats-hemera').errors
const {filter} = require('@allstar/reql-builder')
const sort = require('@allstar/reql-builder/lib/sort')
const TeamMember = require('../../../models/league/team/team-member')
const hemera = require('../../../lib/hemera')
const {Errors, r} = connection.default
const table = TeamMember.getTableName()

module.exports =
hemera.add({
  topic: 'roster'
, cmd: 'update-player'
, version: 'v1'
, auth$: {
    permissions: 'league:roster:update'
  }
}, async function(req) {
  try {
    this.log.debug(`updating team member ${req.allstar_team_member_id}`)
    return await TeamMember
      .get(req.allstar_team_member_id)
      .update({
        number: req.number
      , position_id: req.position
      })
  } catch (e) {
    if (e instanceof Errors.DocumentNotFound) {
      const error = new HemeraError({
        message: `member not found`
      , status: 404
      , code: 'ENOENT'
      , name: 'EntityNotFoundError'
      })
      throw  error
    }
    throw e
  }
})
