'use strict'
/**
 * list available team members
 * @module allstar-league/services/team/roster/list-member
 * @author Eric Satterwhite
 *
 **/

const {connection} = require('zim/lib/db')
const {filter} = require('@allstar/reql-builder')
const sort = require('@allstar/reql-builder/lib/sort')
const TeamMember = require('../../../models/league/team/team-member')
const hemera = require('../../../lib/hemera')
const r = connection.default.r
const table = TeamMember.getTableName()

module.exports =
hemera.add({
  topic: 'roster'
, cmd: 'list'
, version: 'v1'
, auth$: {
    permissions: 'league:roster:read'
  }
}, async function(req) {
  const {limit = 25, offset= 0, filters = {}} = req
  const opts = filter(r, filters)

  return await r.object(
    'meta'
  , r.object(
      'total'
    , r.table(table).filter(opts).count()
    , 'limit'
    , limit
    , 'offset'
    , offset
    , 'next'
    , null
    , 'previous'
    , null
    )
  , 'data'
  , sort(r, r.table(table), {
      field: 'season'
    })
    .filter(opts)
    .skip(offset)
    .limit(limit)
    .coerceTo('array')
  )
})
