'use strict'

/**
 * Add a user to a team roster by
 * @module allstar-league/services/team/roter/add-member
 * @requires zim
 * @requires @allstar/reql-builder
 * @requires allstar-league/models/league/team/team-member
 * @requires allstar-league/lib/hemera
 **/

const {connection} = require('zim/lib/db')
const {filter} = require('@allstar/reql-builder')
const {HemeraError} = require('nats-hemera').errors
const sort = require('@allstar/reql-builder/lib/sort')
const TeamMember = require('../../../models/league/team/team-member')
const LeagueTeam = require('../../../models/league/team/league-team')
const hemera = require('../../../lib/hemera')
const r = connection.default.r
const table = TeamMember.getTableName()

module.exports =
hemera.add({
  topic: 'roster'
, cmd: 'add-player'
, version: 'v1'
, auth$: {
    permissions: 'league:roster:update'
  }
}, async function(req) {
  const {
    league_team_id
  , number
  , user_id
  , position
  } = req

  this.log.info('looking up user ', user_id)
  const user = await this.act({
    topic: 'user'
  , cmd: 'get'
  , version: 'v1'
  , user_id: user_id
  })

  if (!user || !user.active) {
    const error = new HemeraError({
      message: `user ${user_id} not found`
    , status: 404
    , code: 'ENOENT'
    , name: 'EntityNotFoundError'
    })

    throw  error
  }

  this.log.info('saving member record')
  const member = await TeamMember({
      number: number
    , user: user_id
    , league_team_id: league_team_id
    , position_id: position
    }).save()

  let team = null
  try {
    team = await LeagueTeam.get(league_team_id)
  } catch (e) {
    this.log.error(e)
    const error = new HemeraError({
      message: `League Team  Member ${league_team_id} not found`
    , status: 404
    , code: 'ENOENT'
    , name: 'EntityNotFoundError'
    })
    throw  error
  }

  this.log.info('adding activity record', user)
  this.act({
    topic: 'activity'
  , cmd: 'create'
  , pubsub$: true
  , version: 'v1'
  , action: 'added player'
  , content_type: 'user'
  , content_id: user.allstar_auth_user_id
  , extra:  { user }
  , season: (+team.season)
  , creator_type: this.user$.content_type
  , creator_id: this.user$.id
  })
  return member
})
