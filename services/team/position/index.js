'use strict'
/**
 * position service module
 * @module allstar-league/services/team/position
 * @author Eric Satterwhite
 * @since 1.0.0
 * @requires allstar-league/services/team/position/list
 * @auth league:position:read
 **/

module.exports = {
  list: require('./list')
}
