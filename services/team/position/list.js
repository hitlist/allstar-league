'use strict'

/**
 * Lists positions
 * @module allstar-league/services/team/position/list
 * @author Eric Satterwhite
 * @since 1.0.0
 * @requires allstar-league/lib/hemera
 * @requires allstart-league/models/league/team/position
 * @auth league:position:read
 **/

const {connection} = require('zim/lib/db')
const hemera = require('../../../lib/hemera')
const Position = require('../../../models/league/team/position')
const r = connection.default.r
const DB = connection.default._config.db
const TABLE = Position.getTableName()

module.exports =
hemera.add({
  topic: 'position'
, cmd: 'list'
, version: 'v1'
, auth$: {
    permissions: 'league:position:read'
  }
}, function(opts) {
  const {
    offset = 0
  , limit = 50
  , filters = null
  } = opts

  // TODO(esatterwhite)
  // toResponse(r, DB, TABLE, {limit, offset, filters})
  return r.object(
    'meta'
  , r.object(
      'total'
    , r.db(DB).table(TABLE).count()
    , 'limit'
    , limit
    , 'offset'
    , offset
    , 'next'
    , null
    , 'previous'
    , null
    )

  , 'data'
  , r.db(DB).table(TABLE).orderBy({index: r.asc('name')}).coerceTo('array')
  )
})
