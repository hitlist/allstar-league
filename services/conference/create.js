'use strict'

const hemera = require('../../lib/hemera')
const Conference = require('../../models/league/conference')

module.exports =
hemera.add({
  topic: 'conference'
, cmd: 'create'
, version: 'v1'
, auth$: {
    permissions: 'league:conference:create'
  }
}, async function(opts) {
  const conf = await Conference(opts).save()
  this.log.info('new conference created: ', conf.conference_name)
  this.act({
    topic: 'activity'
  , cmd: 'create'
  , version: 'v1'
  , pubsub$: true
  , action: 'create'
  , content_type: conf.contentType()
  , content_id: conf.pk()
  , creator_id: this.user$.id || '00000000-0000-0000-0000-000000000000'
  , creator_type: this.user$.type || 'system'
  , season: null
  , extra: {
      conference_name: conf.conference_name
    }
  })

  return conf
})
