'use strict'

const LeagueTeam = require('../models/league/team/league-team')

new LeagueTeam({
  league_id: '33610e3d-9326-4d01-bddc-9425808b839f'
, conference_id: '0a239f03-c87b-4c54-ac94-5a8741253a4f'
, team_id: '9615df77-3859-4f4e-99b7-b1a2d9d5b216'
, season: 2003
}).save().then((l) => {
  console.log(l.pk())
})
