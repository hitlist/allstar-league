'use strict'

const Stadium = require('../models/stadium')

new Stadium({
  stadium_name: 'Inpro Stadium'
, address: {
    street: 'w181 s18594 Racine Ave'
  , city: 'muskego'
  , state: 'WI'
  , zipcode: '53150'
  }
, location: [32.8875, 87.1291]
, lights: true
, concessions: true
}).save().then((s) => {
  console.log(s.pk())
})
