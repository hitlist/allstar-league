'use strict'

const Team = require('../models/league/team/team')

new Team({
  name: 'Hitmen'
, year_established: 2001
, abbr: 'MSK'
, website: 'http://www.muskegohitmen.com'
, primary: true
, biography: 'This is the hitmen'
, city: 'Muskego'
, state: 'WI'
}).save().then((l) => {
  console.log(l.pk())
})
