'use strict'

const League = require('../models/league/league')

new League({
  abbr: 'IFL'
, league_name: 'Ironman Football League'
, founded_at: new Date('1999-01-01')
, founders: [{
    allstar_auth_user_id: "0b42911d-eb0f-4025-ac4c-6e2c2d4d0c59"
  , name: {
      first:  "joe"
    , last:  "blow"
    }
  }]
}).save().then((l) => {
  console.log(l.pk())
})
