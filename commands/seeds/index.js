'use strict'

module.exports = new Map([
  ['positions', require('./positions')]
, ['teams', require('./teams')]
, ['leagues', require('./leagues')]
])
