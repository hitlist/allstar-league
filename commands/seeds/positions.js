'use strict'

const constants = require('../../lib/constants')

module.exports = () => {
  const Position = require('../../models/league/team/position')
  const records = []
  for ( const [id, position] of constants.POSITIONS.entries()) {
    if (!position.name) {
      continue
    }
    records.push({
      abbr: id
    , name: position.name
    , category: position.category
    })
  }
  return Position.save(records, {conflict: 'replace'})
}
