'use strict'

module.exports = (cb) => {
  const Team = require('../../models/league/team/team')
  return Team.save(TEAMS, {conflict: 'replace'})
}

const TEAMS = [{
  name: 'Hitmen'
, year_established: 2002
, abbr: 'MSK'
, biography: 'The Muskego Hitmen'
, website: 'http://www.muskeoghitmen.com'
, city: 'Muskego'
, state: 'WI'
}, {
  name: 'Mustangs'
, year_established: 2005
, abbr: 'MST'
, biography: 'The Madison Mustangs'
, website: null
, city: 'Madison'
, state: 'WI'
}, {
  name: 'Crusaders'
, year_established: 2000
, abbr: 'CRD'
, biography: 'The Fond Du Lac Crusaders'
, website: null
, city: 'Fond Du Lac'
, state: 'WI'
}, {
  name: 'Sting'
, year_established: 2010
, abbr: 'STG'
, biography: 'The Anitoch String'
, website: null
, city: 'Anitoch'
, state: 'IL'
}, {
  name: 'Rush'
, year_established: 2006
, abbr: 'RSH'
, biography: 'The Roscoe Rush'
, website: null
, city: 'Roscoe'
, state: 'IL'
}, {
  name: 'Rush'
, year_established: 2006
, abbr: 'RSH'
, biography: 'The Roscoe Rush'
, city: 'Roscoe'
, state: 'IL'
}, {
  name: 'Maniacs'
, year_established: 2003
, abbr: 'MAN'
, biography: 'The Milwaukee Maniacs'
, website: null
, city: 'Milwaukee'
, state: 'WI'
}, {
  name: 'Soldiers'
, year_established: 2004
, abbr: 'SLD'
, biography: 'The Waukesha Soldiers'
, website: null
, city: 'Milwaukee'
, state: 'WI'
}, {
  name: 'Venom'
, year_established: 2003
, abbr: 'VNM'
, biography: 'The Milwaukee Venom'
, website: null
, city: 'Milwaukee'
, state: 'WI'
}, {
  name: 'Spartans'
, year_established: 2011
, abbr: 'SPN'
, biography: 'The Wauwatosa Spartans'
, website: null
, city: 'Milwaukee'
, state: 'WI'
}, {
  name: 'Predators'
, year_established: 2005
, abbr: 'PRD'
, biography: 'The West Allis Preditors'
, website: null
, city: 'Milwaukee'
, state: 'WI'
}]

module.exports = () => {
  const Team = require('../../models/league/team/team')
  const records = []

  for ( const [id, team] of Object.entries(TEAMS)) {
    if (!team.name) {
      continue
    }
    records.push(team)
  }
  return Team.save(records, {conflict: 'replace'})
}
