'use strict'

const os = require('os')
const cli = require('seeli')
const seeds = require('./seeds')

module.exports = new cli.Command({
  alias: 'seed'
, description: 'Insert default data into the database'
, usage: [
    'Allstar seed <seed type>'
  , ''
  , cli.bold('Seed Types')
  , cli.bold('==========')
  , cli.colorize(Array.from(seeds.keys()).join(os.EOL))
  ]
, flags: {}
, run: async function(directive, data) {
    if (!seeds.has(directive)) {
      const error = new Error(`Invalid seed type ${directive}. Must be one of ${Array.from(seeds.keys())}`)
      error.name = 'InvalidSeed'
      error.code = 'ERR_SEED'
      throw error
    }

    this.ui.start(`Loading ${directive}...`)
    const seed = seeds.get(directive)
    try {
      await seed(this.ui)
    } catch(err) {
      if (!err.name === 'ValidationError') throw err
      this.ui.fail(`unable to load ${directive} - ${err.message}`)
      throw err
    }
    process.exit(0)
  }
})
