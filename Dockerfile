# 0000 BASE
FROM mhart/alpine-node:8 as base

RUN apk update && apk upgrade && apk add python make g++

RUN addgroup -g 1000 allstar \
  && adduser -u 1000 -G allstar -D -s /bin/sh -h allstar allstar

RUN mkdir -p /opt/app && chown -R allstar:allstar /opt
USER allstar
WORKDIR /opt/app
ARG NPM_TOKEN

COPY package.json package-lock.json /opt/app/

USER allstar

RUN npm install --production

COPY . /opt/app

USER root
RUN find . -user root | grep -E -v '^./node_modules|./.git' | xargs chown allstar:allstar
USER allstar

WORKDIR /opt
RUN tar --exclude=".npm" --exclude=".config" --exclude=".git" --exclude="*.tgz" --exclude="*.tar.gz" --exclude="Dockerfile*" --exclude="*.save" -zcf service.tgz -C /opt app

# RELEASE
FROM mhart/alpine-node:8 as release
RUN addgroup -g 1000 allstar \
  && adduser -u 1000 -G allstar -D -s /bin/sh -h allstar allstar

ENV logger=stdout
ENV log__stdout__level=debug
ENV DEBUG=*

WORKDIR /opt
COPY --from=base /opt/service.tgz /opt
COPY --from=base /usr/bin /usr/bin
COPY --from=base /usr/bin/node /usr/bin/node
COPY --from=base /usr/include/node /usr/include
COPY --from=base /usr/lib/node_modules/ /usr/lib/node_modules/
RUN tar -zxf service.tgz && rm service.tgz
WORKDIR /opt/app
RUN npm link
USER allstar
CMD ["node", "index.js"]
