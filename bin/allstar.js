#!/usr/bin/env node

'use strict'

const seeli = require('seeli')
seeli.set({
  exitOnError: true
, exitOnContent: true
, color: 'yellow'
})

const commands = require('../commands')
for (const [name, command] of commands) {
  seeli.use(name, command)
}

seeli.run()
