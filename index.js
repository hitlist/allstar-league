'use strict'
const util = require('util')
const conf = require('keef')
const hemera = require('./lib/hemera')
const services = require('./lib/fs/loading/service')


if (require.main === module) {
  hemera.ready(() => {
    hemera.log.info('league service started')
    services.load().flat()
  })
}

module.exports = hemera

