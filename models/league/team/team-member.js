'use strict'

/**
 * Represents a Player on a team for a given season
 * @module allstar-league/models/league/team/team-member
 * @author Eric Satterwhite
 * @requires zim
 * @requires gaz/string
 **/

const {connection} = require('zim/lib/db')
const {slugify} = require('gaz/string')
const LeagueTeam = require('./league-team')
const Position = require('./position')

const r = connection.default.r
const fields = connection.default.type

const CONTENT_TYPE = 'team_member'
const DB_TABLE = 'allstar_league_team_member'
const PK = 'allstar_team_member_id'

const TeamMember = connection.default.createModel(DB_TABLE, fields.object().schema({
  allstar_team_member_id: fields.string()
, number: fields.number().min(0).max(99)
, user: fields.string()
, position_id: fields.string().uuid(5)
, position: fields.object().optional()
, league_team_id: fields.string()
, team: fields.object().optional()
}).removeExtra(), {
  pk: PK
, table: {
    shards: 3
  , replicas: 2
  }
})

TeamMember.pre('save', function() {
  this[PK] = r.uuid(`${this.user}:${this.season}:${this.league_team_id}`)
})

TeamMember.ensureIndex('season')
TeamMember.hasOne(LeagueTeam, 'team', 'league_team_id', 'allstar_league_member_id')
TeamMember.hasOne(Position, 'position', 'position_id', 'league_position_id')
module.exports = TeamMember
