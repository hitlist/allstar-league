'use strict'

/**
 * Team related models for league management
 * @module allstar-league/models/league/team
 * @author Eric Satterwhite
 * @requires zim
 * @requires gaz/string
 * @requires allstar/models/league/team/team
 * @requires allstar/services/league/league
 * @requires allstar/services/league/conference
 **/

const {connection} = require('zim/lib/db')
const {slugify} = require('gaz/string')
const Team = require('./team')
const League = require('../league')
const Conference = require('../conference')
const r = connection.default.r
const fields = connection.default.type

const CONTENT_TYPE = 'league_member'
const DB_TABLE = 'allstar_league_member'
const PK = 'allstar_league_member_id'

const LeagueTeam = connection.default.createModel(DB_TABLE, fields.object().schema({
  [PK]: fields.string()
, league_id: fields.string().required()
, team_id: fields.string()
, conference_id: fields.string()
, season: fields.number().integer()
}).removeExtra(), {
  pk: PK
, table: {
    shards: 3
  , replicas: 2
  }
})

LeagueTeam.pre('save', function() {
  this[PK] = r.uuid(`${this.season}:${this.league_id}`)
})

LeagueTeam.defineStatic('pk', function() {
  return PK
})

LeagueTeam.define('pk', function() {
  return this[PK]
})

LeagueTeam.belongsTo(Team, 'team', 'team_id', Team.pk())
LeagueTeam.belongsTo(League, 'league', 'league_id', League.pk())
LeagueTeam.belongsTo(Conference, 'conference', 'conference_id', Conference.pk())

LeagueTeam.ensureIndex('league_id')
LeagueTeam.ensureIndex('team_id')

LeagueTeam.ensureIndex('season_league_idx', function(doc) {
  return [doc('season'), doc('league_id')]
})

LeagueTeam.ensureIndex('season_team_idx', function(doc) {
  return [doc('season'), doc('team_id')]
})

LeagueTeam.ensureIndex('season_league_team_idx', function(doc) {
  return [doc('season'), doc('league_id'), doc('team_id')]
})

module.exports = LeagueTeam
