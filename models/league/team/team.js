'use strict'

/**
 * Team related models for league management
 * @module allstar-league/models/league/team
 * @author Eric Satterwhite
 * @requires zim
 * @requires gaz/string
 **/


const {connection} = require('zim/lib/db')
const {slugify} = require('gaz/string')
const r = connection.default.r
const fields = connection.default.type

const CONTENT_TYPE = 'team'
const DB_TABLE = 'allstar_league_team'
const PK = 'allstar_team_id'

const Team = connection.default.createModel(DB_TABLE, fields.object().schema({
  allstar_team_id: fields.string()
, name: fields.string().required().max(20)
, year_established: fields.number().integer().min(1960).max(9999)
, abbr: fields.string().length(3)
, biography: fields.string().optional().allowNull().default('')
, website: fields.string().optional().default('')
, primary: fields.boolean().optional().default(false)
, slug: fields.string().optional()
, city: fields.string().required().min(1).max(50)
, state: fields.string().required().min(2).max(3)
, archived: fields.boolean().default(() => { return false })
}).removeExtra(), {
  pk: PK
, table: {
    shards: 3
  , replicas: 2
  }
})

Team.pre('save', function() {
  this.slug = slugify(this.name)
})

Team.defineStatic('pk', function() {
  return PK
})

Team.define('pk', function() {
  return this[PK]
})

module.exports = Team
