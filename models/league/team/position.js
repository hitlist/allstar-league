'use strict'

/**
 * Basic Position Model
 * @module allstar-league/models/league/team
 * @author Eric Satterwhite
 * @requires zim
 * @requires allstar-league/lib/constants
 **/


const {connection} = require('zim/lib/db')
const constants = require('../../../lib/constants')
const r = connection.default.r
const fields = connection.default.type

const CONTENT_TYPE = 'position'
const DB_TABLE = 'allstar_league_position'
const PK = 'league_position_id'


const Position = connection.default.createModel(DB_TABLE, fields.object().schema({
  abbr: fields.string()
, name: fields.string().required()
, category: fields.string().enum(Array.from(constants.CATEGORIES.values())).required()
, league_position_id: fields.string().uuid(5)
}).removeExtra(), {
  pk: PK
, table: {
    shards: 3
  , replicas: 2
  }
})

Position.defineStatic('pk', function(){
  return PK
})

Position.define('pk', function() {
  return this[PK]
})

Position.define('model', function() {
  return Position
})

Position.pre('save', function() {
  this.league_position_id = r.uuid(this.abbr)
})

Position.define('contentType', function(){
  return CONTENT_TYPE
})

Position.ensureIndex('category')
Position.ensureIndex('name')
Position.ensureIndex('abbr')
module.exports = Position
