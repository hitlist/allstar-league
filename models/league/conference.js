'use strict'

/**
 * Represents a logical grouping of teams with in a league
 * @module allstar-league/models/league/conference
 * @author Eric Satterwhite
 * @requires zim
 * @requires gaz/string
 **/

const {connection} = require('zim/lib/db')
const {slugify} = require('gaz/string')
const r = connection.default.r
const fields = connection.default.type

const DB_TABLE = 'allstar_league_conference'
const PK = 'allstar_league_conference_id'
const CONTENT_TYPE = 'conference'

const Conference = connection.default.createModel(DB_TABLE, fields.object().schema({
  allstar_league_conference_id: fields.string()
, conference_name: fields.string().required()
, slug: fields.string()
}).removeExtra(), {
  pk: 'allstar_league_conference_id'
, table: {
    shards: 3
  , replicas: 2
  }
})

Conference.pre('save', function() {
  this.slug = slugify(this.conference_name)
})

Conference.defineStatic('pk', function() {
  return PK
})

Conference.define('pk', function() {
  return this.allstar_league_conference_id
})

Conference.define('contentType', function() {
  return CONTENT_TYPE
})

Conference.define('model', function() {
  return Conference
})

module.exports = Conference
