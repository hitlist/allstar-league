'use strict'

/**
 * Represents a single leage containing multiple conferences and teams
 * @module allstar-league/models/league/conference
 * @author Eric Satterwhite
 * @requires zim
 * @requires gaz/string
 **/

const {connection} = require('zim/lib/db')
const {slugify} = require('gaz/string')
const r = connection.default.r
const fields = connection.default.type

const CONTENT_TYPE = 'league'
const DB_TABLE = 'allstar_league'
const PK = 'allstar_league_id'

const League = connection.default.createModel(DB_TABLE, fields.object().schema({
  allstar_league_id: fields.string()
, league_name: fields.string().required()
, abbr: fields.string().required().max(6).min(3)
, created_at: fields.date().default(function() {
    return r.now()
  })
, founded_at: fields.date()
, founders: fields.array().schema(fields.object().schema({
    allstar_auth_user_id: fields.string()
  , name: fields.object().schema({
      first: fields.string()
    , last: fields.string()
    })
  }).removeExtra())
, slug: fields.string()
}).removeExtra(), {
  pk: PK
, table: {
    shards: 3
  , replicas: 2
  }
})

League.pre('save', function() {
  this.slug = slugify(this.league_name)
  this.allstar_league_id = r.uuid(this.league_name.toLowerCase())
  this.founders = Array.isArray(this.founders) ? this.founders: [this.founders]
  this.abbr = this.abbr.toUpperCase()
})

League.defineStatic('pk', function() {
  return PK
})

League.define('pk', function() {
  return this.allstar_league_id
})

League.define('contentType', function() {
  return CONTENT_TYPE
})

League.defineStatic('contentType', function() {
  return CONTENT_TYPE
})

League.define('model', function() {
  return League
})

League.defineStatic('seasons', function(pk, league = null, conference = null) {
  return LeagueTeam
    .getAll(pk, {
      index: 'league_id'
    })
    .group('season')
    .ungroup()
    .map(r.row('group'))
})


League.ensureIndex('allstar_league_id')
League.ensureIndex('founded_at')
League.ensureIndex('founder_id_idx', r.row('founders')('id'))
League.ensureIndex('league_name')
module.exports = League
