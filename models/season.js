'use strict'

const {connection} = require('zim/lib/db')
const r = connection.default.r
const fields = connection.default.type

const Season = connection.default.createModel('allstar_league_season', fields.object().schema({
  year: fields.number().integer().min(1960)
, created_at: fields.date().default(() => {
    return r.now()
  })
}).removeExtra(), {
  pk: 'year'
, table: {
    shards: 3
  , replicas: 2
  }
})

Season.define('pk', function() {
  return this.year
})

Season.define('contentType', function() {
  return 'season'
})

Season.define('model', function() {
  return Season
})

Season.ensureIndex('season')

module.exports = Season
