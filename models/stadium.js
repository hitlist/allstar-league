'use strict'

/**
 * Represents a single physical location where games are played
 * @module allstar-league/models/league/league
 * @author Eric Satterwhite
 * @requires zim
 * @requires gaz/string
 **/

const {connection} = require('zim/lib/db')
const {slugify} = require('gaz/string')
const r = connection.default.r
const fields = connection.default.type

const Stadium = connection.default.createModel('allstar_league_stadium', fields.object().schema({
  stadium_name: fields.string().max(100).required()
, allstar_league_stadium_id: fields.string()
, location: fields.point().optional().allowNull().default(null)
, lights: fields.boolean().default(false)
, concessions: fields.boolean().default(false)
, address: fields.object().schema({
    city: fields.string().required()
  , street: fields.string().required()
  , state: fields.string().max(2).min(2)
  , zipcode: fields.number().integer()
  }).required().removeExtra()
, coordinates: fields.virtual().default(function() {
    if(!this.location) return null
    return this.location.coordinates
  })
, created_at: fields.date().default(function() {
    return r.now()
  })
, slug: fields.string()
}).removeExtra(), {
  pk: 'allstar_league_stadium_id'
, table: {
    shards: 3
  , replicas: 2
  }
})

Stadium.pre('save', function() {
  this.slug = slugify(this.stadium_name)
})

Stadium.define('pk', function() {
  return this.allstar_league_stadium_id
})

Stadium.define('contentType', function() {
  return 'stadium'
})

Stadium.define('model', function() {
  return Stadium
})

Stadium.ensureIndex('slug')
Stadium.ensureIndex('location', {geo: true})

module.exports = Stadium
